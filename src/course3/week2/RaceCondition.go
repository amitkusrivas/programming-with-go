package main

import (
	"fmt"
	"time"
)

/*
Write two goroutines which have a race condition when executed concurrently. Explain what the race condition is
and how it can occur.
*/
var x = 0

func main() {

	fmt.Println("Before running go routine value of x is ", x)

	go incrementX()
	go printX()

	time.Sleep(time.Second * 2)
}

func incrementX() {
	x = x + 1
}

func printX() {
	fmt.Println(x)
}
