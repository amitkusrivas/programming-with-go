package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

/*
Write a program to sort an array of integers. The program should partition the array into 4 parts, each of which is
sorted by a different goroutine. Each partition should be of approximately equal size. Then the main goroutine should
merge the 4 sorted subarrays into one large sorted array.

The program should prompt the user to input a series of integers. Each goroutine which sorts 1\4 of the array should print
the subarray that it will sort. When sorting is complete, the main goroutine should print the entire sorted list.
*/
func main() {
	var intSlice = make([]int, 0, 10)

	var scanner = bufio.NewScanner(os.Stdin)

	fmt.Print("Enter atleast 4 numbers separated by space: ")
	scanner.Scan()

	for _, strValue := range strings.Split(scanner.Text(), " ") {
		intValue, _ := strconv.Atoi(strValue)
		intSlice = append(intSlice, intValue)
	}

	var sortGoRoutineChan = make(chan []int, 4)

	partitionSize := len(intSlice) / 4
	go sortSubArray(append([]int(nil), intSlice[0:partitionSize]...), sortGoRoutineChan)
	go sortSubArray(append([]int(nil), intSlice[partitionSize:2*partitionSize]...), sortGoRoutineChan)
	go sortSubArray(append([]int(nil), intSlice[2*partitionSize:3*partitionSize]...), sortGoRoutineChan)
	go sortSubArray(append([]int(nil), intSlice[3*partitionSize:]...), sortGoRoutineChan)

	// Creat final slice with length and capacity equal to input arr
	var sortedArr = make([]int, 0, len(intSlice))

	// Merge 4 sorted sub array into final array
	for i := 0; i < 4; i++ {
		sortedArr = mergeSortedSubArr(sortedArr, <-sortGoRoutineChan)
	}

	fmt.Println("Final sorted array: ", sortedArr)
}

func mergeSortedSubArr(sortedArr []int, sortedSubArr []int) []int {

	var newSortedArr = make([]int, len(sortedArr)+len(sortedSubArr))

	indexFirstArr := 0  // Initial index of first subarray
	indexSecondArr := 0 // Initial index of second subarray
	indexMergedArr := 0 // Initial index of merged subarray

	for indexFirstArr < len(sortedArr) && indexSecondArr < len(sortedSubArr) {
		if sortedArr[indexFirstArr] <= sortedSubArr[indexSecondArr] {
			newSortedArr[indexMergedArr] = sortedArr[indexFirstArr]
			indexFirstArr++
		} else {
			newSortedArr[indexMergedArr] = sortedSubArr[indexSecondArr]
			indexSecondArr++
		}

		indexMergedArr++
	}

	/* Copy the remaining elements from sortedArr, if there are any */
	for indexFirstArr < len(sortedArr) {
		newSortedArr[indexMergedArr] = sortedArr[indexFirstArr]
		indexFirstArr++
		indexMergedArr++
	}

	/* Copy the remaining elements of sortedSubArr, if there are any */
	for indexSecondArr < len(sortedSubArr) {
		newSortedArr[indexMergedArr] = sortedSubArr[indexSecondArr]
		indexSecondArr++
		indexMergedArr++
	}

	return newSortedArr
}

/*
 Run in separate go routine to sort the subarray.
*/
func sortSubArray(subArrSlice []int, channel chan []int) {
	fmt.Println(subArrSlice)
	BubbleSort(subArrSlice)
	channel <- subArrSlice
}

// Function to bubble sort the slice passed.
func BubbleSort(slice []int) {
	n := len(slice)
	sorted := false

	for !sorted {
		swapped := false

		for i := 0; i < n-1; i++ {
			if slice[i] > slice[i+1] {
				slice[i], slice[i+1] = slice[i+1], slice[i]
				swapped = true
			}
		}

		if !swapped {
			sorted = true
		}

		n = n - 1
	}
}
