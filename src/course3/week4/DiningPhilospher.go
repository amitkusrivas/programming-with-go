package main

import (
	"fmt"
	"sync"
	"time"
)

/*
Implement the dining philosopher’s problem with the following constraints/modifications.

1. There should be 5 philosophers sharing chopsticks, with one chopstick between each adjacent pair of philosophers.
2. Each philosopher should eat only 3 times (not in an infinite loop as we did in lecture)
3. The philosophers pick up the chopsticks in any order, not lowest-numbered first (which we did in lecture).
4. In order to eat, a philosopher must get permission from a host which executes in its own goroutine.
5. The host allows no more than 2 philosophers to eat concurrently.
6. Each philosopher is numbered, 1 through 5.
7. When a philosopher starts eating (after it has obtained necessary locks) it prints “starting to eat <number>” on a
line by itself, where <number> is the number of the philosopher.
8. When a philosopher finishes eating (before it has released its locks) it prints “finishing eating <number>” on a
line by itself, where <number> is the number of the philosopher.
*/
type ChopStick struct{ sync.Mutex }

type Philosopher struct {
	philosopherId    int
	eatCountMutex    sync.Mutex
	eatCount         int
	isEating         bool
	eatingFlagMutex  sync.Mutex
	leftCS           *ChopStick
	rightCS          *ChopStick
	leftNeighbor     *Philosopher
	rightNeighbor    *Philosopher
	wantToEatChannel chan bool
}

func main() {
	chopSticks := make([]*ChopStick, 5)

	// Initialize Chopsticks
	for i := 0; i < 5; i++ {
		chopSticks[i] = new(ChopStick)
	}
	fmt.Println("Initialized Chopsticks")

	// Initialize Philosopher
	philosophers := make([]*Philosopher, 5)
	for i := 0; i < 5; i++ {
		philosophers[i] = &Philosopher{philosopherId: i + 1, eatCount: 0, isEating: false, leftCS: chopSticks[i],
			rightCS: chopSticks[(i+1)%5], wantToEatChannel: make(chan bool)}
	}

	// Initializing the neighbours of the philosophers
	for i := 0; i < 5; i++ {
		var leftNeighbourIndex = 0
		if i == 0 {
			leftNeighbourIndex = 4
		} else {
			leftNeighbourIndex = i - 1
		}

		var rightNeighbourIndex = 0
		if i == 4 {
			rightNeighbourIndex = 0
		} else {
			rightNeighbourIndex = i + 1
		}

		philosophers[i].leftNeighbor = philosophers[leftNeighbourIndex]
		philosophers[i].rightNeighbor = philosophers[rightNeighbourIndex]
	}
	fmt.Println("Initialized Philosophers")

	var doneChannel = make(chan bool)

	// Starting the philosophers
	for i := 0; i < 5; i++ {
		go philosophers[i].wantToEat(doneChannel)
	}

	/*
		Host the philosophers.
		The host functions will return the philosophers who can eat on the channel.
		Host will let each philosophers eat exactly three times. In addition, after all 5 philosophers is done eating three
		times, host will close the channel.
	*/
	for eatingPhilo := range hostPhilosopher(philosophers, doneChannel) {
		fmt.Printf("Letting philosopher %d to eat\n", eatingPhilo.philosopherId)
		go eatingPhilo.eat()
	}
}

/*
Philosopher want to eat, so send the communication to the host on the wantToEatChannel which is dedicated for each
philosopher. This is done three times and each time host will block the philosopher until it is ready to eat.

When philosopher is done eating three times, it will send the signal on the done channel.
*/
func (p *Philosopher) wantToEat(doneChannel chan bool) {

	/* Eat at most three times. */
	for {
		p.eatCountMutex.Lock()
		philoEatCount := p.eatCount
		p.eatCountMutex.Unlock()
		if philoEatCount < 2 {
			/*
				Trying to find from host routine if Philosopher can eat. Only ask if philosopher is not
				eating
			*/
			time.Sleep(time.Second * 5)

			p.eatingFlagMutex.Lock()
			if !p.isEating {
				p.eatingFlagMutex.Unlock()
				p.wantToEatChannel <- true
			} else {
				p.eatingFlagMutex.Unlock()
			}
		} else {
			break
		}
	}

	doneChannel <- true
}

/*
 Philosopher is ready to eat and so it will take control of the left and right chopstick. When done eating, will
release the chopsticks.
*/
func (p *Philosopher) eat() {
	p.leftCS.Lock()
	p.rightCS.Lock()
	p.eatCountMutex.Lock()
	p.eatingFlagMutex.Lock()
	fmt.Println("starting to eat", p.philosopherId)
	p.eatCount = p.eatCount + 1
	p.isEating = true

	fmt.Println(p.philosopherId, "eating")

	fmt.Println("finishing eating", p.philosopherId)
	p.isEating = false
	p.eatingFlagMutex.Unlock()
	p.eatCountMutex.Unlock()
	p.rightCS.Unlock()
	p.leftCS.Unlock()
}

/*
	Host the philosophers.
	The host functions will return the philosophers who can eat on the channel.
	Host will let each philosophers eat exactly three times. In addition, after all 5 philosophers is done eating three
	times, host will close the channel.
*/
func hostPhilosopher(philosophers []*Philosopher, doneChannel chan bool) chan *Philosopher {
	// Host will make sure only two philosopher can eat at one time.
	c := make(chan *Philosopher, 2)

	// Check if the Philosopher left and right philosopher is eating. In case yes, host will not allow the
	// philosopher to eat.
	var canPhilosopherEat = func(philosopher *Philosopher) bool {
		philosopher.leftNeighbor.eatingFlagMutex.Lock()
		philosopher.rightNeighbor.eatingFlagMutex.Lock()
		if philosopher.leftNeighbor.isEating || philosopher.rightNeighbor.isEating {
			return false
		}
		defer philosopher.rightNeighbor.eatingFlagMutex.Unlock()
		defer philosopher.leftNeighbor.eatingFlagMutex.Unlock()

		return true
	}

	// Run the host as go routine so that it can decide on which philosopher to eat.
	go func() {
		var donePhilosopherCount = 0

		for {
			select {
			case <-philosophers[0].wantToEatChannel:
				fmt.Printf("Philosopher %d wants to eat\n", philosophers[0].philosopherId)
				if canPhilosopherEat(philosophers[0]) {
					c <- philosophers[0]
				}
			case <-philosophers[1].wantToEatChannel:
				fmt.Printf("Philosopher %d wants to eat\n", philosophers[1].philosopherId)
				if canPhilosopherEat(philosophers[1]) {
					c <- philosophers[1]
				}
			case <-philosophers[2].wantToEatChannel:
				fmt.Printf("Philosopher %d wants to eat\n", philosophers[2].philosopherId)
				if canPhilosopherEat(philosophers[2]) {
					c <- philosophers[2]
				}
			case <-philosophers[3].wantToEatChannel:
				fmt.Printf("Philosopher %d wants to eat\n", philosophers[3].philosopherId)
				if canPhilosopherEat(philosophers[3]) {
					c <- philosophers[3]
				}
			case <-philosophers[4].wantToEatChannel:
				fmt.Printf("Philosopher %d wants to eat\n", philosophers[4].philosopherId)
				if canPhilosopherEat(philosophers[4]) {
					c <- philosophers[4]
				}
			case <-doneChannel:
				donePhilosopherCount++
				fmt.Printf("%d Philosophers done eating\n", donePhilosopherCount)
				if donePhilosopherCount == 5 {
					fmt.Println("All Philosophers done eating")
					close(c)
					break
				}
			}
		}
	}()

	return c
}
