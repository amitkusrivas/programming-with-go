package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
)

/*
Write a program which prompts the user to first enter a name, and then enter an address. Your program should create a map
and add the name and address to the map using the keys “name” and “address”, respectively. Your program should use Marshal()
to create a JSON object from the map, and then your program should print the JSON object.
*/
func main() {

	var nameAddMap map[string]string
	nameAddMap = make(map[string]string)

	var scanner = bufio.NewScanner(os.Stdin)

	fmt.Print("Enter a name: ")
	scanner.Scan()
	var nameStr = scanner.Text()

	nameAddMap["name"] = nameStr

	fmt.Print("Enter a address: ")
	scanner.Scan()
	var addressStr = scanner.Text()

	nameAddMap["address"] = addressStr

	var nameAddJson, err = json.Marshal(nameAddMap)

	if err != nil {
		fmt.Printf("Error occured while converting map to Json: %v \n", err)
	}

	fmt.Println(string(nameAddJson))
}
