package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
Write a program which reads information from a file and represents it in a slice of structs. Assume that there is a text
file which contains a series of names. Each line of the text file has a first name and a last name, in that order,
separated by a single space on the line.

Your program will define a name struct which has two fields, fname for the first name, and lname for the last name.
Each field will be a string of size 20 (characters).

Your program should prompt the user for the name of the text file. Your program will successively read each line of the
text file and create a struct which contains the first and last names found in the file. Each struct created will be added
to a slice, and after all lines have been read from the file, your program will have a slice containing one struct for
each line in the file. After reading all lines from the file, your program should iterate through your slice of structs
and print the first and last names found in each struct.
*/
func main() {

	type Name struct {
		fname string
		lname string
	}

	var scanner = bufio.NewScanner(os.Stdin)

	fmt.Print("Enter a file name relative to working directory : ")
	scanner.Scan()
	var fileName = scanner.Text()

	var filePtr, err = os.Open(fileName)

	if err != nil {
		fmt.Printf("Error occured while reading the file: %v \n", err)
	} else {
		var fileScanner = bufio.NewScanner(filePtr)
		defer filePtr.Close()

		fileScanner.Split(bufio.ScanLines)

		var nameSlice []Name

		for fileScanner.Scan() {
			nameStrArr := strings.Split(fileScanner.Text(), " ")
			nameSlice = append(nameSlice, Name{nameStrArr[0], nameStrArr[1]})
		}

		for _, name := range nameSlice {
			fmt.Println(name.fname + " " + name.lname)
		}
	}
}
