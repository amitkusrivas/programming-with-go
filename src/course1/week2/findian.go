package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
Write a program which prompts the user to enter a string. The program searches through the entered string for the
characters ‘i’, ‘a’, and ‘n’. The program should print “Found!” if the entered string starts with the character ‘i’,
ends with the character ‘n’, and contains the character ‘a’. The program should print “Not Found!” otherwise. The
program should not be case-sensitive, so it does not matter if the characters are upper-case or lower-case.
*/
func main() {
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Print("Enter string for scanning: ")
	scanner.Scan() // use `for scanner.Scan()` to keep reading
	strToScan := scanner.Text()

	isIasPrefix := strings.HasPrefix(strToScan, "i") || strings.HasPrefix(strToScan, "I")
	isNasSuffix := strings.HasSuffix(strToScan, "n") || strings.HasSuffix(strToScan, "N")
	isAInStr := strings.Contains(strToScan, "a") || strings.Contains(strToScan, "A")

	if isIasPrefix && isNasSuffix && isAInStr {
		fmt.Print("Found!")
	} else {
		fmt.Print("Not Found!")
	}
}
