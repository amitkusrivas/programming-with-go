package main

import "fmt"

func main() {

	var numFloat float64

	fmt.Printf("Enter floating point number: ")
	_, _ = fmt.Scan(&numFloat)

	fmt.Print(int(numFloat))
}
