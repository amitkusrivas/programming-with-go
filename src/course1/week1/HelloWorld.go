package main

import "fmt"

func main() {

	// TYpe aliasing
	type Celsius float64

	var dayTemp Celsius = 50.3

	// declaration of variable
	var wondering = "abc"

	fmt.Printf("Hello World\n")
	fmt.Printf(wondering + "\n")
	fmt.Printf("Day temprature %0.2f", dayTemp)
}
