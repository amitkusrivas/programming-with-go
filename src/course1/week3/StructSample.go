package main

import "fmt"

type Person struct {
	name string
	age  int
}

func main() {
	var joe Person
	var jammy Person

	// other way to initialize Struct
	jane := new(Person)
	jane.name = "Jane"
	jane.age = 23
	jhonny := Person{
		name: "Jhonny",
		age:  19,
	}

	fmt.Println(joe, jammy, *jane, jhonny)
}
