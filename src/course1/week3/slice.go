package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
)

/*
Write a program which prompts the user to enter integers and stores the integers in a sorted slice. The program should be
written as a loop. Before entering the loop, the program should create an empty integer slice of size (length) 3.
During each pass through the loop, the program prompts the user to enter an integer to be added to the slice. The program
adds the integer to the slice, sorts the slice, and prints the contents of the slice in sorted order. The slice must grow
in size to accommodate any number of integers which the user decides to enter. The program should only quit (exiting the
loop) when the user enters the character ‘X’ instead of an integer.
*/
func main() {
	var intSlice = make([]int, 0, 3)

	var scanner = bufio.NewScanner(os.Stdin)

	var valueEntered string
	for {
		fmt.Print("Enter a number: ")
		scanner.Scan()
		valueEntered = scanner.Text()

		if valueEntered == "X" {
			break
		}

		intValue, _ := strconv.Atoi(valueEntered)

		intSlice = append(intSlice, intValue)
		sort.Ints(intSlice)

		fmt.Println(intSlice)
	}
}
