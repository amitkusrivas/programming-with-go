package main

import (
	"fmt"
)

func main() {
	var slice = []string{"a", "b"}
	fmt.Printf("Type %T\n", slice)

	var arrInt = [...]string{"a", "b"}
	fmt.Printf("Type %T\n", arrInt)

	sliceUsingMake := make([]string, 10)
	fmt.Printf("\n ", sliceUsingMake)
	fmt.Printf("\n ", sliceUsingMake[1])

	// Hash table
	var hashTable = map[string]int{"joe": 123}

	fmt.Println(hashTable)
	fmt.Println(hashTable["jane"])

	// delete the key value pair from the map
	delete(hashTable, "jane")
	delete(hashTable, "joe")

	fmt.Println(hashTable)
	fmt.Println(hashTable["jane"])

	hashTable["wonder"] = 123

	// you can test presence of the key using two value assignment
	value, isKeyPresent := hashTable["jane"]
	fmt.Println(value, isKeyPresent)

	value, isKeyPresent = hashTable["wonder"]
	fmt.Println(value, isKeyPresent)

	// len is used to find out the length of the map

	// iterate through the map
	for key, val := range hashTable {
		fmt.Println(key, val)
	}
}
