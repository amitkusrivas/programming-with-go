package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

/*
Write a Bubble Sort program in Go. The program should prompt the user to type in a sequence of up to 10 integers.
The program should print the integers out on one line, in sorted order, from least to greatest. Use your favorite
search tool to find a description of how the bubble sort algorithm works.

As part of this program, you should write a function called BubbleSort() which takes a slice of integers as an argument
and returns nothing. The BubbleSort() function should modify the slice so that the elements are in sorted order.

A recurring operation in the bubble sort algorithm is the Swap operation which swaps the position of two adjacent elements
in the slice. You should write a Swap() function which performs this operation. Your Swap() function should take two
arguments, a slice of integers and an index value i which indicates a position in the slice. The Swap() function
should return nothing, but it should swap the contents of the slice in position i with the contents in position i+1.
*/
func main() {
	var intSlice = make([]int, 0, 10)

	var scanner = bufio.NewScanner(os.Stdin)

	fmt.Print("Enter number separated by one space: ")
	scanner.Scan()

	for _, strValue := range strings.Split(scanner.Text(), " ") {
		intValue, _ := strconv.Atoi(strValue)
		intSlice = append(intSlice, intValue)
	}

	BubbleSort(intSlice)

	fmt.Println(intSlice)
}

// Function to bubble sort the slice passed.
func BubbleSort(slice []int) {
	n := len(slice)
	sorted := false

	for !sorted {
		swapped := false

		for i := 0; i < n-1; i++ {
			if slice[i] > slice[i+1] {
				Swap(slice, i)
				swapped = true
			}
		}

		if !swapped {
			sorted = true
		}

		n = n - 1
	}
}

func Swap(slice []int, swapIndex int) {
	slice[swapIndex], slice[swapIndex+1] = slice[swapIndex+1], slice[swapIndex]
}
