package main

import "fmt"

func main() {
	var sliceOfNumbers = []int{1, 2, 3}

	fmt.Printf("Type of slice: %T \n", sliceOfNumbers)

	fmt.Println(addAllSliceNumbers(sliceOfNumbers))
}

func addAllSliceNumbers(slice []int) int {
	sum := 0
	for _, num := range slice {
		sum += num
	}

	return sum
}
