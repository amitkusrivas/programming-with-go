package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
)

/*
Write a program which first prompts the user to enter values for acceleration, initial velocity, and initial displacement.
Then the program should prompt the user to enter a value for time and the program should compute the displacement after
the entered time.
*/
func main() {
	var scanner = bufio.NewScanner(os.Stdin)

	fmt.Print("Enter acceleration: ")
	scanner.Scan()
	var acceleration, _ = strconv.ParseFloat(scanner.Text(), 64)

	fmt.Print("Enter velocity: ")
	scanner.Scan()
	var velocity, _ = strconv.ParseFloat(scanner.Text(), 64)

	fmt.Print("Enter initial displacement: ")
	scanner.Scan()
	var displacement, _ = strconv.ParseFloat(scanner.Text(), 64)

	distanceFunction := GenDisplaceFn(acceleration, velocity, displacement)

	fmt.Print("Enter time covered: ")
	scanner.Scan()
	var time, _ = strconv.ParseFloat(scanner.Text(), 64)

	fmt.Println("Distance covered after time T : ", distanceFunction(time))
}

func GenDisplaceFn(acceleration, velocity, displacement float64) func(time float64) float64 {
	return func(time float64) float64 {
		distanceAfterT := 0.5*acceleration*math.Pow(time, 2.0) + (velocity * time) + displacement
		return distanceAfterT
	}
}
