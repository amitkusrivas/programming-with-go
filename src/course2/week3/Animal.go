package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
Write a program which allows the user to get information about a predefined set of animals. Three animals are predefined,
cow, bird, and snake. Each animal can eat, move, and speak. The user can issue a request to find out one of three things
about an animal: 1) the food that it eats, 2) its method of locomotion, and 3) the sound it makes when it speaks. The
following table contains the three animals and their associated data which should be hard-coded into your program.
*/
type Animal struct {
	food       string
	locomotion string
	noise      string
}

func (animal Animal) Eat() {
	fmt.Println(animal.food)
}

func (animal Animal) Move() {
	fmt.Println(animal.locomotion)
}

func (animal Animal) Speak() {
	fmt.Println(animal.noise)
}

func main() {
	animalMap := map[string]Animal{
		"cow":   {"grass", "walk", "moo"},
		"bird":  {"worms", "fly", "peep"},
		"snake": {"mice", "slither", "hss"},
	}

	actionMap := map[string]func(Animal){
		"eat":   Animal.Eat,
		"move":  Animal.Move,
		"speak": Animal.Speak,
	}

	var scanner = bufio.NewScanner(os.Stdin)

	for {
		fmt.Print("> ")
		scanner.Scan()
		animalActionSlice := strings.Split(scanner.Text(), " ")

		var animal = animalMap[animalActionSlice[0]]
		var action = actionMap[animalActionSlice[1]]

		action(animal)
	}
}
