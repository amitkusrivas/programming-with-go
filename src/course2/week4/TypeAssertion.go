package main

import "fmt"

type Action interface {
	Eat()
	Move()
	Speak()
}

//type Animal struct {
//	food       string
//	locomotion string
//	noise      string
//}
//
//func NewAnimal(food string, locomotion string, noise string) *Animal {
//	return &Animal{food: food, locomotion: locomotion, noise: noise}
//}
//
//func (animal Animal) Eat() {
//	fmt.Println("Eat animal")
//}
//
//func (animal Animal) Move() {
//	fmt.Println("Move animal")
//}
//
//func (animal Animal) Speak() {
//	fmt.Println("Speak animal")
//}

type Mammal struct {
	food     string
	movement string
	language string
}

func NewMammal(food string, movement string, language string) *Mammal {
	return &Mammal{food: food, movement: movement, language: language}
}

func (mammal Mammal) Eat() {
	fmt.Println("Eat Mammal")
}

func (mammal Mammal) Move() {
	fmt.Println("Move Mammal")
}

func (mammal Mammal) Speak() {
	fmt.Println("Speak Mammal")
}

func main() {
	var actionable Action

	switch sh := actionable.(type) {
	case Animal:
		sh.Move()
	case Mammal:
		sh.Move()
	}
}
