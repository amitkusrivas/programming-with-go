package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

/*
Write a program which allows the user to create a set of animals and to get information about those animals. Each animal
has a name and can be either a cow, bird, or snake. With each command, the user can either create a new animal of one of
the three types, or the user can request information about an animal that he/she has already created. Each animal has a
unique name, defined by the user. Note that the user can define animals of a chosen type, but the types of animals are
restricted to either cow, bird, or snake. The following table contains the three types of animals and their associated
data.
*/
type Animal interface {
	Eat()
	Move()
	Speak()
}

type Cow struct {
	food       string
	locomotion string
	noise      string
}

func (c Cow) Eat() {
	fmt.Println("grass")
}

func (c Cow) Move() {
	fmt.Println("walk")
}

func (c Cow) Speak() {
	fmt.Println("moo")
}

type Bird struct {
	food       string
	locomotion string
	noise      string
}

func (b Bird) Eat() {
	fmt.Println("worms")
}

func (b Bird) Move() {
	fmt.Println("fly")
}

func (b Bird) Speak() {
	fmt.Println("peep")
}

type Snake struct {
	food       string
	locomotion string
	noise      string
}

func (s Snake) Eat() {
	fmt.Println("mice")
}

func (s Snake) Move() {
	fmt.Println("slither")
}

func (s Snake) Speak() {
	fmt.Println("hsss")
}

func main() {
	animalTypeMap := map[string]Animal{
		"cow":   Cow{},
		"bird":  Bird{},
		"snake": Snake{},
	}

	actionMap := map[string]func(Animal){
		"eat":   Animal.Eat,
		"move":  Animal.Move,
		"speak": Animal.Speak,
	}

	var animalMap = make(map[string]Animal)

	var scanner = bufio.NewScanner(os.Stdin)

	for {
		fmt.Print("> ")
		scanner.Scan()
		animalActionSlice := strings.Split(scanner.Text(), " ")

		switch animalActionSlice[0] {
		case "newanimal":
			name := animalActionSlice[1]
			animalType := animalActionSlice[2]

			animalMap[name] = animalTypeMap[animalType]
			fmt.Println("Created it!")
		case "query":
			var name = animalActionSlice[1]
			var action = animalActionSlice[2]
			var actionFn = actionMap[action]
			actionFn(animalMap[name])
		}
	}
}
